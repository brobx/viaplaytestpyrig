const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const trailersRouter = require('./routes/trailers');
const handleError = require('./helpers/errorHelper');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/trailers', trailersRouter);

app.use((err, req, res, next) => {
  handleError(err, req, res);
});

module.exports = app;
