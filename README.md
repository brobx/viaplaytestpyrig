# Pavlo Pyrig Viaplay test task

# Description
API to get film trailer by viaplay link.
# Requirements
  - Node.js 9.x or higher
  - Npm 5.x or higher

# How to use
```sh
$ npm install
$ npm start
```
It will start on port 3000
Make GET request to `http://localhost:3000/trailers?resourceLink=${ViaplayResourceLink}`
ViaplayResourceLink for example `https://content.viaplay.se/pc-se/film/the-commuter-2018`
It will response with Object
`{
    "url": "https://www.youtube.com/watch?v=iQZL28DywEA"
}`

# Tests
Run
```sh
$ npm test
```

# Limits
Project use clusters, so it will use all CPUs.
Unfortunatelly TMBD API has limit 40 requests every 10 seconds for free accounts. To get round this limit we can use different accounts for TMDB api, use Daily File Exports or get movie trailers for all viaplay films and save them to local database.