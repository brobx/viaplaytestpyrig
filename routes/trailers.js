const express = require('express');
const controller = require('../controllers/trailers');

const router = express.Router();

/* GET film trailer. */
router.get('/', controller.getTrailer);

module.exports = router;
