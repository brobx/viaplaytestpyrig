/* eslint-disable consistent-return */
const viaplayService = require('../services/viaplayService');
const movieService = require('../services/movieDbService');

exports.getTrailer = async (req, res, next) => {
  try {
    const { resourceLink } = req.query;
    const validateResourceLink = viaplayService.validateResourceLink(resourceLink);
    if (!validateResourceLink) {
      return next({
        statusCode: 400,
        message: 'Not valid resourceLink',
      });
    }
    const resourceImdbId = await viaplayService.getResourceImdbId(resourceLink);
    const tmdbId = await movieService.getFilmDbId(resourceImdbId);
    if (!tmdbId) {
      return next({
        statusCode: 404,
      });
    }
    const trailerUrl = await movieService.getTrailerLink(tmdbId);
    if (!trailerUrl) {
      return next({
        statusCode: 404,
      });
    }
    res.status(200).send({
      url: trailerUrl,
    });
  } catch (err) {
    return next(err);
  }
};
