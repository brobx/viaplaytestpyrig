/* eslint-disable */
const viaplayService = require('../services/viaplayService');
const {expect} = require('chai');

const wrongResourceLink = 'https://content.testplay.se/pc-se/film/the-commuter-2018';
const viaplayResourceLink = 'https://content.viaplay.se/pc-se/film/the-commuter-2018';
const viaplayFilmObject = {
  _embedded: {
    'viaplay:blocks': [
      {_embedded: {
        'viaplay:product': {
          content: {
            imdb: {
              id: 'test111111'
            }
          }
        }
      }}
    ]
  }
}

const WrongViaplayFilmObject = {
  _embedded: {
    'viaplay:blocks': [
      {_embedded: {}}
    ]
  }
}

describe('#validateResourceLink()', () => {

  context('With not viaplay link', function() {
    it('should return false', function() {
      expect(viaplayService.validateResourceLink(wrongResourceLink)).to.be.false;
    });
  });
  
  context('With viaplay link', function() {
    it('should return true', function() {
      expect(viaplayService.validateResourceLink(viaplayResourceLink)).to.be.true;
    });
  });
  
});

describe('#getImdbId()', () => {

  context('With wrong viaplay object', function() {
    it('should return false', function() {
      expect(viaplayService.getImdbId(WrongViaplayFilmObject)).to.be.false;
    });
  });
  
  context('With viaplay link', function() {
    it('should return true', function() {
      expect(viaplayService.getImdbId(viaplayFilmObject)).to.equal('test111111');
    });
  });

});