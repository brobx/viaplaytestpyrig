/* eslint-disable no-restricted-syntax */
const helpers = require('../helpers/requestHelper');

module.exports = {

  /**
   * @description Find film data by IMDB id and return TMDB id
   * @param {String} imdbId
   * @returns {String} TMDB film id
   */
  async getFilmDbId(imdbId) {
    const searchUrl = `${process.env.TMDB_API_URL}find/${imdbId}?api_key=${process.env.TMDB_API_KEY}&external_source=imdb_id`;
    const tmdbData = await helpers.makeRequest(searchUrl, 'GET', null);
    for (const category of Object.keys(tmdbData)) {
      if (tmdbData[category].length > 0) {
        return tmdbData[category][0].id;
      }
    }
    return false;
  },

  /**
   * @description Get TMDB film data and get YouTube trailer key
   * @param {String} tmdbId
   * @returns {String} trailerYouTubeLink
   */
  async getTrailerLink(tmdbId) {
    const searchUrl = `${process.env.TMDB_API_URL}movie/${tmdbId}/videos?api_key=${process.env.TMDB_API_KEY}`;
    const tmdbVideoData = await helpers.makeRequest(searchUrl, 'GET', null);
    const trailerKeys = tmdbVideoData.results.filter((item) => {
      if (item.type === 'Trailer' && item.site === 'YouTube') {
        return true;
      }
      return false;
    });
    if (trailerKeys.length === 0) {
      return false;
    }
    const trailerYouTubeLink = `https://www.youtube.com/watch?v=${trailerKeys[0].key}`;
    return trailerYouTubeLink;
  },
};
