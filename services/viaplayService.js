/* eslint-disable no-underscore-dangle */
const url = require('url');

const helpers = require('../helpers/requestHelper');

module.exports = {

  /**
   * @description Get Imdb id from viaplay object
   * @param {Object} resourceData
   * @returns {String} filmImdbId
   */
  getImdbId(resourceData) {
    try {
      const filmImdbId = resourceData._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id;
      return filmImdbId;
    } catch (err) {
      return false;
    }
  },

  /**
   * @description Get Viaplay film object
   * @param {String} filmresourceLinkUrl
   * @returns {String} imdbId
   */
  async getResourceImdbId(filmresourceLinkUrl) {
    const resourceData = await helpers.makeRequest(filmresourceLinkUrl, 'GET', null);
    const imdbId = this.getImdbId(resourceData);
    return imdbId;
  },

  /**
   * @description Validate viaplay resource link.
   * @param {String} filmresourceLinkUrl
   * @returns {Boolean}
   */
  validateResourceLink(filmresourceLinkUrl) {
    const urlData = url.parse(filmresourceLinkUrl);
    if (urlData.host !== 'content.viaplay.se') {
      return false;
    }
    return true;
  },
};
