const request = require('request-promise');

module.exports = {

  /**
   * @description helper for http requests
   * @param {String} uri
   * @param {String} method
   * @param {Object} body
   * @returns {*} data from resource
   */
  async makeRequest(uri, method, body) {
    const options = {
      method,
      uri,
      body,
    };
    const data = await request(options);
    return JSON.parse(data);
  },
};
