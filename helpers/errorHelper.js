const handleError = (err, req, res) => {
  let { statusCode, message } = err;
  if (statusCode === 404) {
    message = 'Cannot find trailer link';
  }
  statusCode = statusCode || 500;
  res.status(statusCode).json({
    status: 'error',
    statusCode,
    message,
  });
};

module.exports = handleError;
